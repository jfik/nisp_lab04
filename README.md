# Programowanie w języku C

Język C - strukturalny język programowania.

## Pierwszy program

```c
int main()
{
    printf("Hello World!\n");
    return 0;
}
```

## Kompilacja projektu

```bash
gcc main.c
```

## Uruchomienie projektu

```bash
./a.out
```

## Autor

Jan Kowalski

## Wikipedia
[O języku C](https://pl.wikipedia.org/wiki/C_(język_programowania))
